from django.db import models


class Result(models.Model):
    query = models.fields.CharField(max_length=255, null=False, verbose_name='тест запроса')
    position = models.fields.IntegerField(verbose_name='позиция в выдаче')
