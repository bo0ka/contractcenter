import requests
import lxml.html as lh

from django.core.management.base import BaseCommand
from django.conf import settings

from case.models import Result


class Command(BaseCommand):
    help = 'Результаты запроса к Яндексу заносятся в табличку'

    def add_arguments(self, parser):
        parser.add_argument('query', type=str, help='словосочетание запроса')
        parser.add_argument('site', type=str, help='искомый сайт')

    def handle(self, *args, **options):

        url = 'https://yandex.ru/search/'
        query, site = options['query'], options['site']
        # первые 30 урлов выдачи по словосочетанию
        sites = []

        session = requests.Session()
        session.headers.update(settings.YA_HEADER)
        # выдача с 0ой страницы поэтому просто range
        for page in range(3):
            # запрос страницы
            r = session.get(url, params='text={query}&p={page}'.format(query=query, page=page))
            # нет ответа, просто продолжим
            if r.status_code != 200:
                continue
            content = r.content  # open('result.txt', 'wb').write(content)
            page = lh.fromstring(content)
            # получили капчу, сообщим и прекратим работу
            if page.xpath('//title')[0].text == 'Ой!':
                abnormal = page.body[0].find('p')
                self.stdout.write(self.style.ERROR(abnormal.text))
                return
            # получим по xpath домены из выдачи
            sites.extend([str(e) for e in page.xpath('//ul//li//h2//a/@href')])
        # сохраним искомые домены в нумерованный список
        matched = list(filter(lambda x: site in x[1], enumerate(sites)))
        # если что то совпало запишем в базу
        if matched:
            for _ in matched:
                Result.objects.create(query=query, position=_[0])
            # сообщим в консоль
            self.stdout.write(self.style.SUCCESS('Добавили %s записей' % len(matched)))
